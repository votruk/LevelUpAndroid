package ru.kurtov.levelupandroid.lesson8.lateinititems

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import ru.kurtov.levelupandroid.lesson8.R

class LateInitActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

        val adapter = LateInitAdapter()
        recyclerView.adapter = adapter

        adapter.setOnRecyclerClicked(object : LateInitAdapter.OnRecyclerClicked {
            override fun onClick(text: String) {
                Toast.makeText(this@LateInitActivity, text, Toast.LENGTH_SHORT).show()
            }
        })

        val items = (1..100).map { i -> "Поздний элемент #$i" }
        adapter.setItems(items)
    }

}