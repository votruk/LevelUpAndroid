package ru.kurtov.levelupandroid.lesson8

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import ru.kurtov.levelupandroid.lesson8.acitivityclicks.ActivityClicksActivity
import ru.kurtov.levelupandroid.lesson8.adapterclicks.AdapterClicksActivity
import ru.kurtov.levelupandroid.lesson8.lateinititems.LateInitActivity
import ru.kurtov.levelupandroid.lesson8.multipleitems.MultipleItemsActivity
import ru.kurtov.levelupandroid.lesson8.threeitems.ThreeItemsActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val threeItems = findViewById<Button>(R.id.three_items)
        val multipleItems = findViewById<Button>(R.id.multiple_items)
        val adapterClicks = findViewById<Button>(R.id.adapter_click)
        val activityClicks = findViewById<Button>(R.id.activity_click)
        val lateInitItems = findViewById<Button>(R.id.late_init_items)

        threeItems.setOnClickListener { openNewScreen(ThreeItemsActivity::class.java) }
        multipleItems.setOnClickListener { openNewScreen(MultipleItemsActivity::class.java) }
        adapterClicks.setOnClickListener { openNewScreen(AdapterClicksActivity::class.java) }
        activityClicks.setOnClickListener { openNewScreen(ActivityClicksActivity::class.java) }
        lateInitItems.setOnClickListener { openNewScreen(LateInitActivity::class.java) }
    }

    private fun openNewScreen(classToOpen: Class<*>) {
        startActivity(Intent(this, classToOpen))
    }

}
