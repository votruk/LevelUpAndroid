package ru.kurtov.levelupandroid.lesson8

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

class RecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val textView: TextView = itemView.findViewById(R.id.recycler_text)

    fun bind(textToShow: String) {
        textView.text = textToShow
    }

}