package ru.kurtov.levelupandroid.lesson8.acitivityclicks

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import ru.kurtov.levelupandroid.lesson8.R

class ActivityClicksActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

        val items = (1..100).map { i -> "Элемент #$i" }
        val adapter = ActivityClicksAdapter(items)
        recyclerView.adapter = adapter

        adapter.setOnRecyclerClicked(object : ActivityClicksAdapter.OnRecyclerClicked {
            override fun onClick(text: String) {
                Toast.makeText(this@ActivityClicksActivity, text, Toast.LENGTH_SHORT).show()
            }

        })
    }

}