package ru.kurtov.levelupandroid.lesson8.lateinititems

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ru.kurtov.levelupandroid.lesson8.R
import ru.kurtov.levelupandroid.lesson8.RecyclerViewHolder

class LateInitAdapter : RecyclerView.Adapter<RecyclerViewHolder>() {

    private var onRecyclerClicked: LateInitAdapter.OnRecyclerClicked? = null
    private val items: MutableList<String> = mutableListOf()

    fun setOnRecyclerClicked(onRecyclerClicked: LateInitAdapter.OnRecyclerClicked?) {
        this.onRecyclerClicked = onRecyclerClicked
    }

    fun setItems(newItems: List<String>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater
                .from(parent.context).inflate(R.layout.item_recycler_view_holder, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val textToShow = items[position]
        holder.bind(textToShow)
        holder.itemView.setOnClickListener {
            onRecyclerClicked?.onClick(textToShow)
        }
    }

    interface OnRecyclerClicked {
        fun onClick(text: String)
    }

}