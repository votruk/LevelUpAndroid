package ru.kurtov.levelupandroid.lesson8.threeitems

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import ru.kurtov.levelupandroid.lesson8.R
import ru.kurtov.levelupandroid.lesson8.RecyclerAdapter

class ThreeItemsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

        val items = listOf<String>("Первый элемент", "Второй элемент", "Третий элемент")
        val adapter = RecyclerAdapter(items)
        recyclerView.adapter = adapter
    }

}