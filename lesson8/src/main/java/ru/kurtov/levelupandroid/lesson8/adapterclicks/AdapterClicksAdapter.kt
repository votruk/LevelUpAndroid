package ru.kurtov.levelupandroid.lesson8.adapterclicks

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import ru.kurtov.levelupandroid.lesson8.R
import ru.kurtov.levelupandroid.lesson8.RecyclerViewHolder

class AdapterClicksAdapter(private val items: List<String>) : RecyclerView.Adapter<RecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater
                .from(parent.context).inflate(R.layout.item_recycler_view_holder, parent, false)
        return RecyclerViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val textToShow = items[position]
        holder.bind(textToShow)
        holder.itemView.setOnClickListener {
            Toast.makeText(holder.itemView.context, textToShow, Toast.LENGTH_SHORT).show()
        }
    }

}