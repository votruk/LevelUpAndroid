package ru.kurtov.levelupandroid.dynamic

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.kurtov.levelupandroid.LevelUpFragment
import ru.kurtov.levelupandroid.R

class DynamicFragmentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dynamic_fragment)

        if (savedInstanceState == null) {
            val fragmentTransition = getSupportFragmentManager().beginTransaction()
            fragmentTransition.replace(R.id.fragment_container, LevelUpFragment())
            fragmentTransition.commit()
        }
    }

}