package ru.kurtov.levelupandroid.args

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.kurtov.levelupandroid.ArgsFragment
import ru.kurtov.levelupandroid.R

class ArgsFragmentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dynamic_fragment)

        if (savedInstanceState == null) {
            val fragmentTransition = supportFragmentManager.beginTransaction()
            fragmentTransition.replace(R.id.fragment_container, ArgsFragment.newInstance("Hola amigo!"))
            fragmentTransition.commit()
        }
    }

}