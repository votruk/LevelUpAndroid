package ru.kurtov.levelupandroid.static

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.kurtov.levelupandroid.R

class StaticFragmentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_static_fragment)
    }

}