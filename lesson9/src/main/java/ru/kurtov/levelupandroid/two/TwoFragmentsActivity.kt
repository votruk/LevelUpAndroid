package ru.kurtov.levelupandroid.two

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.kurtov.levelupandroid.ArgsFragment
import ru.kurtov.levelupandroid.R

class TwoFragmentsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_two_fragments)

        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.top_fragment_container,
                            ArgsFragment.newInstance("TOP FRAGMENT"))
                    .commit()

            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.bottom_fragment_container,
                            ArgsFragment.newInstance("BOTTOM FRAGMENT"))
                    .commit()
        }
    }
}